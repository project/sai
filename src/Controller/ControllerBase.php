<?php declare(strict_types=1);

namespace Drupal\sai\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Controller\ControllerBase as DrupalControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal SAI does not have a PHP API.
 */
abstract class ControllerBase extends DrupalControllerBase {

  protected static $jsonapiObject = [
    'version' => '1.1',
  ];

  protected static $defaultResponseHeaders = [
    'content-type' => 'application/vnd.api+json',
  ];

  private $hashSalt;

  protected function getNoContentResponse(): Response {
    return Response::create(null, Response::HTTP_NO_CONTENT);
  }

  protected function getErrorResponse(int $status, string $detail): Response {
    return JsonResponse::create([
      'errors' => [[
        'status' => sprintf('%s %s', $status, Response::$statusTexts[$status]),
        'detail' => $detail,
      ]]
    ], $status, static::$defaultResponseHeaders);
  }

  /**
   * Hashes a link using its href and its target attributes, if any.
   *
   * This method generates an unpredictable, but deterministic, 7 character
   * alphanumeric hash for a given link.
   *
   * The hash is unpredictable because a random hash salt will be used for every
   * request. The hash is deterministic because, within a single request, links
   * with the same href and target attributes (i.o.w. duplicates) will generate
   * equivalent hash values.
   *
   * @param array $link
   *   A link to be hashed.
   *
   * @return string
   *   A 7 character alphanumeric hash.
   */
  protected function getLinkHash(array $link) {
    // Generate a salt unique to each instance of this class.
    if (!$this->hashSalt) {
      $this->hashSalt = Crypt::randomBytesBase64();
    }
    // Hash the string.
    $b64_hash = Crypt::hashBase64($this->hashSalt . Json::encode($link));
    // Remove any dashes and underscores from the base64 hash and then return
    // the first 7 characters.
    return substr(str_replace(['-', '_'], '', $b64_hash), 0, 7);
  }

}
