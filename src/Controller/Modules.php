<?php declare(strict_types=1);

namespace Drupal\sai\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\InfoParserException;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\sai\LinkRelations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class Modules extends ControllerBase {

  const SCOPE_ALL = 'all';

  const SCOPE_INSTALLED = 'installed';

  const SCOPE_UNINSTALLED = 'uninstalled';

  protected $moduleList;

  protected $moduleHandler;

  protected $moduleInstaller;

  public function __construct(ModuleExtensionList $module_list, ModuleHandlerInterface $module_handler, ModuleInstallerInterface $module_installer) {
    $this->moduleList = $module_list;
    $this->moduleHandler = $module_handler;
    $this->moduleInstaller = $module_installer;
  }

  public function detail(Request $request): Response {
    $modules = $this->getModules(self::SCOPE_ALL);
    $machine_name = $request->get('module');
    if (!isset($modules[$machine_name])) {
      return $this->getErrorResponse(Response::HTTP_NOT_FOUND, sprintf('The %s module is not available.', $machine_name));
    }
    $document = [
      'jsonapi' => static::$jsonapiObject,
      'data' => $this->getModuleAsJsonApiData($modules[$machine_name]),
      'links' => [
        'self' => [
          'href' => $request->getUri(),
        ],
      ]
    ];
    return JsonResponse::create($document, 200, static::$defaultResponseHeaders);
  }

  public function list(Request $request, RouteMatchInterface $route_match): Response {
    $modules = $this->getModuleData($route_match->getParameter('module_scope'));
    $document = [
      'jsonapi' => static::$jsonapiObject,
      'data' => [
        'type' => 'moduleList',
        'id' => Url::fromRoute('sai.module.list')->setAbsolute()->toString(),
        'attributes' => [
          'title' => 'Modules',
        ],
        'relationships' => [
          'items' => [
            'data' => array_map(function (array $module_data) {
              return array_intersect_key($module_data, array_flip(['type', 'id']));
            }, $modules),
            'links' => [
              'self' => [
                'href' => Url::fromRoute(sprintf('%s.relationships.items', $route_match->getRouteName()))->setAbsolute()->toString(),
              ],
            ],
          ],
        ],
        'links' => [
          'self' => [
            'href' => Url::fromRoute('sai.module.list')->setAbsolute()->toString(),
          ],
        ],
      ],
      'included' => $modules,
      'links' => [
        'self' => [
          'href' => $request->getUri(),
        ],
      ]
    ];
    return JsonResponse::create($document, 200, static::$defaultResponseHeaders);
  }

  public function items(Request $request, RouteMatchInterface $route_match): Response {
    $modules = $this->getModuleData($route_match->getParameter('module_scope'));
    return JsonResponse::create([
      'jsonapi' => static::$jsonapiObject,
      'data' => array_map(function (array $module_data) {
        return array_intersect_key($module_data, array_flip(['type', 'id']));
      }, $modules),
      'links' => [
        'self' => [
          'href' => $request->getUri(),
        ],
      ]
    ], 200, static::$defaultResponseHeaders);
  }

  public function install(Request $request) {
    $body = (string) $request->getContent();
    if ($request->headers->get('content-type') !== 'application/vnd.api+json') {
      return $this->getErrorResponse(Response::HTTP_UNSUPPORTED_MEDIA_TYPE, 'The request payload must be use the JSON:API media type.');
    }
    $document = Json::decode($body);
    if (empty($document['data'][0]['type']) || empty($document['data'][0]['id'])) {
      return $this->getErrorResponse(Response::HTTP_BAD_REQUEST, "The JSON:API request document's data must contain a resource identifier.");
    }
    $operation = $request->getMethod() === 'POST' ? 'install' : 'uninstall';
    $modules = [];
    foreach ($document['data'] as $identifier) {
      $path = parse_url($identifier['id'])['path'];
      $url = Url::fromUri(sprintf('internal:%s', $path));
      $modules[] = $url->getRouteParameters()['module'];
    }
    $before = array_keys($this->getModules($operation === 'install' ? self::SCOPE_INSTALLED : self::SCOPE_UNINSTALLED));
    if ($operation === 'install') {
      $this->moduleInstaller->install($modules);
      $expected = array_flip(array_flip(array_merge($before, $modules)));
      $after = array_keys($this->getModules($operation === 'install' ? self::SCOPE_INSTALLED : self::SCOPE_UNINSTALLED));
      $needs_response = !empty(array_diff($after, $expected));
    }
    else {
      $this->moduleInstaller->uninstall($modules);
      $expected = array_diff($before, $modules);
      $after = array_keys($this->getModules($operation === 'install' ? self::SCOPE_INSTALLED : self::SCOPE_UNINSTALLED));
      $needs_response = !empty(array_diff($expected, $after));
    }
    if (!$needs_response) {
      return $this->getNoContentResponse();
    }
    $self = parse_url($request->getUri());
    $get_request = Request::create($self['path']);
    return $this->items($get_request, RouteMatch::createFromRequest($get_request));
  }

  protected function getModuleAsJsonApiData(Extension $module): array {
    $module_is_installed = $this->moduleHandler->moduleExists($module->getName());
    $data = [
      'type' => 'module',
      'id' => Url::fromRoute('sai.module.detail', [
        'module' => $module->getName(),
      ])->setAbsolute()->toString(),
      'attributes' => [
        'name' => $module->info['name'],
        'description' => (string) $this->t($module->info['description']),
        'version' => $module->info['version'],
        'installationState' => (string) $this->t($module_is_installed ? 'Installed' : 'Not installed'),
      ],
      'links' => [
        'self' => [
          'href' => Url::fromRoute('sai.module.detail', [
            'module' => $module->getName(),
          ])->setAbsolute()->toString(),
        ],
      ],
    ];
    $data['links'][$module_is_installed ? 'uninstall' : 'install'] = [
      'href' => Url::fromRoute('sai.module.list.install')->setAbsolute()->toString(),
      'rel' => $module_is_installed ? LinkRelations::MODULE_UNINSTALL : LinkRelations::MODULE_INSTALL,
      'do:method' => $module_is_installed ? 'DELETE' : 'POST',
      'do:data' => [[
        'type' => $data['id'], 'id' => $data['id'],
      ]],
    ];
    return $data;
  }

  /**
   * @param string $scope
   * @return array
   */
  protected function getModuleData(string $scope): array {
    return array_map(function (Extension $module) {
      return $this->getModuleAsJsonApiData($module);
    }, array_values($this->getModules($scope)));
  }

  /**
   * @param string $scope
   *   The scope of modules to return. One of: 'all', 'installed', or
   *   'uninstalled'.
   *
   * @return \Drupal\Core\Extension\Extension[]
   */
  protected function getModules(string $scope): array {
    assert(in_array($scope, [self::SCOPE_ALL, self::SCOPE_INSTALLED, self::SCOPE_UNINSTALLED], TRUE));
    // Include system.admin.inc so we can use the sort callbacks.
    $this->moduleHandler->loadInclude('system', 'inc', 'system.admin');
    // Sort all modules by their names.
    try {
      // The module list needs to be reset so that it can re-scan and include
      // any new modules that may have been added directly into the filesystem.
      $modules = $this->moduleList->reset()->getList();
      uasort($modules, 'system_sort_modules_by_info_name');
    }
    catch (InfoParserException $e) {
      //$this->messenger()->addError($this->t('Modules could not be listed due to an error: %error', ['%error' => $e->getMessage()]));
      $modules = [];
    }
    return array_filter($modules, function (Extension $module) use ($scope) {
      $module_is_installed = $this->moduleHandler->moduleExists($module->getName());
      return $scope === self::SCOPE_ALL || ($scope === self::SCOPE_INSTALLED XOR !$module_is_installed);
    });
  }

}
