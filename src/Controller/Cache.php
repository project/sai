<?php declare(strict_types=1);

namespace Drupal\sai\Controller;

use Symfony\Component\HttpFoundation\Response;

final class Cache extends ControllerBase {

  public function rebuild(): Response {
    drupal_flush_all_caches();
    return $this->getNoContentResponse();
  }

}
