<?php declare(strict_types=1);

namespace Drupal\sai\Controller;

use Drupal\Core\Url;
use Drupal\sai\LinkRelations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class EntryPoint extends ControllerBase {

  public function index() {
    if ($this->currentUser()->isAnonymous()) {
      return $this->unauthenticatedResponse();
    }
    return $this->indexResponse();
  }

  protected function indexResponse(): Response {
    $document = [
      'jsonapi' => static::$jsonapiObject,
      'data' => [
        'type' => 'entryPoint',
        'id' => Url::fromRoute('sai.index')->setAbsolute()->toString(),
        'links' => [
          'self' => Url::fromRoute('sai.index')->setAbsolute()->toString(),
        ]
      ],
      'links' => [
        'cacheRebuild' => [
          'href' => Url::fromRoute('sai.cache.rebuild')->setAbsolute()->toString(),
          'rel' => LinkRelations::CACHE_REBUILD,
          'do:method' => 'POST',
        ],
        'moduleList' => [
          'href' => Url::fromRoute('sai.module.list')->setAbsolute()->toString(),
          'rel' => LinkRelations::MODULE_LIST,
        ],
      ],
    ];
    return JsonResponse::create($document, 200, static::$defaultResponseHeaders);
  }

  protected function unauthenticatedResponse(): Response {
    return JsonResponse::create([
      'jsonapi' => static::$jsonapiObject,
      'data' => [
        'type' => 'entryPoint',
        'id' => Url::fromRoute('sai.index')->setAbsolute()->toString(),
        'links' => [
          'self' => Url::fromRoute('sai.index')->setAbsolute()->toString(),
        ]
      ],
      'links' => [
        'authenticate' => [
          'href' => Url::fromRoute('user.login.http', [], [
            'query' => [
              '_format' => 'json',
            ],
          ])->setAbsolute()->toString(),
          'type' => 'application/json',
        ],
      ],
    ], 200, static::$defaultResponseHeaders);
  }

}
