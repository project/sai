<?php declare(strict_types=1);

namespace Drupal\sai;

final class LinkRelations {

  const CACHE_REBUILD = 'https://www.drupal.org/project/sai/rels/cache-rebuild';

  const MODULE_LIST = 'https://www.drupal.org/project/sai/rels/module-list';

  const MODULE = 'https://www.drupal.org/project/sai/rels/module';

  const MODULE_INSTALL = 'https://www.drupal.org/project/sai/rels/module-install';

  const MODULE_UNINSTALL = 'https://www.drupal.org/project/sai/rels/module-uninstall';

}
